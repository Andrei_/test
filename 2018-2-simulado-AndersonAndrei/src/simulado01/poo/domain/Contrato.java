package simulado01.poo.domain;

import java.util.ArrayList;
import java.util.List;

public class Contrato {
	private Integer numContrato;
	private String nomeCliente;
	private String enderecoCliente;
	private List veiculos;
	private Integer vlrContrato;
    
	public Contrato(String nomeCliente, String enderecoCliente){
    	this.nomeCliente = nomeCliente;
    	this.enderecoCliente = enderecoCliente;
    }
	private String getNomeCliente() {
		return nomeCliente;
	}

	private void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	private String getEnderecoCliente() {
		return enderecoCliente;
	}

	private void setEnderecoCliente(String enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}

	private Integer getVlrContrato() {
		return vlrContrato;
	}

	private void setVlrContrato(Integer vlrContrato) {
		this.vlrContrato = vlrContrato;
	}

}
