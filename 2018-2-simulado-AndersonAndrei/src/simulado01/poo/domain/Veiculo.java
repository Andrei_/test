package simulado01.poo.domain;

public class Veiculo {
	private String placa;
	private Integer anoFabricacao;
    private TipoVeiculoEnum tipoVeiculo;
	public Veiculo(String placa, Integer anoFabricacao, TipoVeiculoEnum tipoVeiculo) {
		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.tipoVeiculo = tipoVeiculo;

	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public TipoVeiculoEnum getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeiculoEnum tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

}
