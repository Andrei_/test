package simulado01.poo.persistence;

import java.util.ArrayList;
import java.util.List;

import simulado01.poo.domain.Veiculo;

public class VeiculoDAO {
	private List<Veiculo> veiculos = new ArrayList();

	public String adicionarVeiculo(Veiculo veiculo) {
		veiculos.add(veiculo);
		return null;
	}

	public List<Veiculo> getList() {
		return veiculos;
	}

	public Veiculo findByPlaca(String string) {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca().equalsIgnoreCase(string)) {
				return veiculo;
			}
		}
		return null;
	}
}
