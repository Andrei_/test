package simulado01.poo.tui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import simulado01.poo.business.VeiculoBO;
import simulado01.poo.domain.TipoVeiculoEnum;
import simulado01.poo.domain.Veiculo;

public class VeiculoTui {
	Scanner sc = new Scanner(System.in);
	VeiculoBO veiculoBO = new VeiculoBO();

	public boolean adicionarVeiculo(Veiculo veiculo) {
		String placa = getString("Informe a placa do ve�culo: ");
		Integer anoFabricacao = getInteger("Informe o ano de fabrica��o do ve�culo: ");
		TipoVeiculoEnum tipo = getTipo("Informe o tipo do Ve�culo");
		veiculo = veiculoBO.findByPlaca(placa);
		if (veiculo == null) {
			veiculo = new Veiculo(placa, anoFabricacao, tipo);
			try {
				veiculoBO.adicionarVeiculo(veiculo);
			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			return true;
		}
		return false;
	}

	private TipoVeiculoEnum getTipo(String string) {
		while (true) {
			System.out.println(string);
			String tipoString = sc.nextLine();
			try {
				TipoVeiculoEnum tipoVeiculo = TipoVeiculoEnum.valueOf(tipoString.toUpperCase());
				return tipoVeiculo;
			} catch (IllegalArgumentException e) {
				System.out.println("Tipo informado n�o v�lido ");
			}
		}
	}

	public boolean removerVeiculo(String placa) {
		List<Veiculo> veiculos = veiculoBO.getList();
		String string = getString("Informe qual Ve�culo deseja remover: ");
		Veiculo veiculo = veiculoBO.findByPlaca(string);
		if (veiculo == null) {
			System.out.println("Ve�culo inexistente!!!");
			return false;
		} else {
			veiculos.remove(veiculo);
			return true;
		}
	}

	public ArrayList<Veiculo> consultarVeiculos() {

		return (ArrayList<Veiculo>) veiculoBO.getList();
	}

	public ArrayList<Veiculo> consultarVeiculos(String tipoVeiculo) {
        
		return null;
	}

	public Veiculo consultarVeiculo(String placa) {
		placa = getString("Qual ve�culo deseja buscar ?");
		Veiculo veiculo = veiculoBO.findByPlaca(placa);
		if (veiculo == null) {

		}
		return veiculo;
	}

	public String getString(String string) {
		System.out.println(string);
		return sc.nextLine();
	}

	public Integer getInteger(String string) {
		System.out.println(string);
		sc.nextInt();
		sc.nextLine();
		return sc.nextInt();
	}
}
