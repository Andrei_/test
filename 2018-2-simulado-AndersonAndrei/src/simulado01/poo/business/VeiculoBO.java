package simulado01.poo.business;

import java.util.List;

import simulado01.poo.domain.Veiculo;
import simulado01.poo.persistence.VeiculoDAO;

public class VeiculoBO {
	private VeiculoDAO veiculoDAO = new VeiculoDAO();

	public String adicionarVeiculo(Veiculo veiculo) throws Exception {
		validarVeiculo(veiculo);
		return veiculoDAO.adicionarVeiculo(veiculo);
	}

	public void validarVeiculo(Veiculo veiculo) throws Exception{
		validarPlaca(veiculo.getPlaca(), "Placa");
	}

	public void validarPlaca(String placa, String string) throws Exception {
       if(placa.trim().isEmpty()) {		
    	   throw new Exception("Voc� deixou o " + string + "em branco");
       }
	}

	public List<Veiculo> getList() {
		return veiculoDAO.getList();
	}

	public Veiculo findByPlaca(String string) {
		return veiculoDAO.findByPlaca(string);
	}
	
}
